package main.java.mip.tema1.exercitiul1;

import java.util.ArrayList;
import main.java.mip.tema1.exercitiul1.movie.Artistic;
import main.java.mip.tema1.exercitiul1.movie.aabstract.Movie;
import main.java.mip.tema1.exercitiul1.movie.Serial;
import main.java.mip.tema1.exercitiul1.populator.MoviePopulator;



public class Exercitiul1 {

	public static ArrayList<Movie> movies = new ArrayList<Movie>();

	public static void howManyMoviesFromEachCategoryAndTheLongestOneFromEach() {

		int artisticMovieCounter = 0;
		int serialMovieCounter = 0;

		int longestArtisticMovieIndex = -1;
		int longestSerialMovieIndex = -1;

		int maxDurationArtisticMovie = 0;
		int maxDurationSerialMovie = 0;

		int movieCounter = -1;

		for (Movie movie : movies) {
			movieCounter++;
			
			if (movie instanceof Artistic) {
				
				artisticMovieCounter++;
				Artistic currentArtisticMovie = (Artistic) movie;
				if (currentArtisticMovie.getDuration() > maxDurationArtisticMovie) {
					longestArtisticMovieIndex = movieCounter;
					maxDurationArtisticMovie = currentArtisticMovie.getDuration();
				}
				
			} else if (movie instanceof Serial) {
				
				serialMovieCounter++;
				Serial currentSerialMovie = (Serial) movie;
				int serialDuration = currentSerialMovie.getNumberOfSeasons() * currentSerialMovie.getNumberOfEpisodes()
						* currentSerialMovie.getEpisodeDuration();
				
				if (serialDuration > maxDurationSerialMovie) {
					maxDurationSerialMovie = serialDuration;
					longestSerialMovieIndex = movieCounter;
				}
			}
		}
		
		System.out.println("The number of artistic movies is " + artisticMovieCounter + ".");
		System.out.println("The number of serial movies is  " + serialMovieCounter + ".");
		System.out.println();
		System.out.println("The longest artistic movie is " + movies.get(longestArtisticMovieIndex).getName() +
				" and it lasts " + maxDurationArtisticMovie + " minutes.");
		System.out.println("The longest serial movie is " + movies.get(longestSerialMovieIndex).getName() +
				" and it lasts " + maxDurationSerialMovie + " minutes.");
	}

	public static void main(String[] args) {

		MoviePopulator.generateRandomMovies(movies);
		howManyMoviesFromEachCategoryAndTheLongestOneFromEach();

	}
}
