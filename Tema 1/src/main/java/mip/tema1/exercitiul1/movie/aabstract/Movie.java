package main.java.mip.tema1.exercitiul1.movie.aabstract; //can't name the folder "abstract" because Java would 
														//see it as a reserved word...

import main.java.mip.tema1.exercitiul1.enums.Genre;
import main.java.mip.tema1.exercitiul1.enums.MovieType;

public abstract class Movie {
	
	public Movie() {} 
	
	protected Long id;
	
	protected String name;
	
	protected Genre genre;
	
	protected MovieType movieType;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Genre getGenre() {
		return genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	public MovieType getMovieType() {
		return movieType;
	}
	public void setMovieType(MovieType movieType) {
		this.movieType = movieType;
	}
	
	
}
