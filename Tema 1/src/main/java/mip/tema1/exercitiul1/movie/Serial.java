package main.java.mip.tema1.exercitiul1.movie;

import main.java.mip.tema1.exercitiul1.enums.Genre;
import main.java.mip.tema1.exercitiul1.enums.MovieType;
import main.java.mip.tema1.exercitiul1.movie.aabstract.Movie;

public class Serial extends Movie{

	public Serial() {}
	
	public Serial(Long id, String name, Genre genre, MovieType movieType,int numberOfSeasons,
			int numberOfEpisodes, int episodeDurationInMinutes) {
		this.id =id;
		this.name = name;
		this.genre = genre;
		this.movieType = movieType;
		this.numberOfSeasons = numberOfSeasons;
		this.numberOfEpisodes = numberOfEpisodes;
		this.episodeDurationInMinutes = episodeDurationInMinutes;
	}
	
	private int numberOfSeasons;
	
	private int numberOfEpisodes;
	
	private int episodeDurationInMinutes;
	
	public int getNumberOfEpisodes() {
		return numberOfEpisodes;
	}

	public void setNumberOfEpisodes(int numberOfEpisodes) {
		this.numberOfEpisodes = numberOfEpisodes;
	}

	public int getEpisodeDuration() {
		return episodeDurationInMinutes;
	}

	public void setEpisodeDuration(int episodeDurationInMinutes) {
		this.episodeDurationInMinutes = episodeDurationInMinutes;
	}


	public int getNumberOfSeasons() {
		return numberOfSeasons;
	}

	public void setNumberOfSeasons(int numberOfSeasons) {
		this.numberOfSeasons = numberOfSeasons;
	}
	
}
