package main.java.mip.tema1.exercitiul1.movie;

import main.java.mip.tema1.exercitiul1.enums.Genre;
import main.java.mip.tema1.exercitiul1.enums.MovieType;
import main.java.mip.tema1.exercitiul1.movie.aabstract.Movie;

public class Artistic extends Movie{
		
	private int durationInMinutes;

	public Artistic() {}
	
	public Artistic(Long id, String name, Genre genre, MovieType movieType, int durationInMinutes) {
		this.id =id;
		this.name = name;
		this.genre = genre;
		this.movieType = movieType;
		this.durationInMinutes = durationInMinutes;
	}
	
	public int getDuration() {
		return durationInMinutes;
	}

	public void setDuration(int durationInMinutes) {
		this.durationInMinutes = durationInMinutes;
	}
	
}
