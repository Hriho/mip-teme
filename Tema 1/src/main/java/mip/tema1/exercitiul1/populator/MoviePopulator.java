package main.java.mip.tema1.exercitiul1.populator;

import java.util.ArrayList;

import main.java.mip.tema1.exercitiul1.enums.Genre;
import main.java.mip.tema1.exercitiul1.enums.MovieType;
import main.java.mip.tema1.exercitiul1.movie.Artistic;
import main.java.mip.tema1.exercitiul1.movie.aabstract.Movie;
import main.java.mip.tema1.exercitiul1.movie.Serial;

public class MoviePopulator {

	public static void generateRandomMovies(ArrayList<Movie> movies) {

		 Long serialId = 0L;
		 Long artisticId = 0L;
		
		for (int i = 0; i < 100; i++) {
			if ((i * 23 - 11 * 8 / 2) % 2 == 0) {
				Movie movie = new Serial(++serialId, "Serial" + serialId,
						i % 3 == 0 ? Genre.COMEDY : (i % 3 == 1 ? Genre.HORROR : Genre.SF), MovieType.SERIAL,
						i % 10 + 1, i % 25 + 10, (i * 43) % 60);
				movies.add(movie);
			} else {
				Movie movie = new Artistic(++artisticId, "Artistic" + artisticId,
						i % 3 == 0 ? Genre.COMEDY : (i % 3 == 1 ? Genre.HORROR : Genre.SF), MovieType.ARTISTIC,
						(i * 43) % 150);
				movies.add(movie);
			}
		}
	}
	
}
