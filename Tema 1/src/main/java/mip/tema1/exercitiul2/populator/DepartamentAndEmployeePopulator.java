package main.java.mip.tema1.exercitiul2.populator;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import main.java.mip.tema1.exercitiul2.company.departament.Departament;
import main.java.mip.tema1.exercitiul2.company.employee.Employee;
import main.java.mip.tema1.exercitiul2.enums.DepartamentName;

public class DepartamentAndEmployeePopulator {

	public static void populateDepartamentAndEmployeeLists(ArrayList<Employee> employees, ArrayList<Departament> departaments) {
		
		Long id = 0L;
	
		
		departaments.add(new Departament(1L,DepartamentName.ACCOUNTING));
		departaments.add(new Departament(2L,DepartamentName.LAW));
		departaments.add(new Departament(3L,DepartamentName.MANAGEMENT));
		
		String[] names = {"Hrihoreanu Radu", "Ivan Bogdan", "Heres Beatrice", "Bratosin Alexandru", "Bivol Stefan-Silvano",
				"Solca Ghita", "Todirenchi Elena", "Cozma Daniel", "Cretu Radu", "Szep Cristian"};
		
		String [] cnps = {"2981212154048","1880317459381","1841130221539","1991109186241","1951118158691",
				"2760509396795","2741006108959","2730629440662","1960828167204","1661026282114"};
		
		Double[] salarys = {115.66, 199.24, 210.55, 301.00, 206.11, 199.24, 301.00, 450.21, 610.24, 111.11};
		
		for(int i=0;i<10;i++) {
			employees.add(new Employee(++id,names[i],cnps[i],salarys[i],departaments.get(i%3),
					new GregorianCalendar(2008+i, i , 13).getTime()));
		}
		System.out.println("INFO: Employees and Departaments has been populated.");
	}
	
}
