package main.java.mip.tema1.exercitiul2.company;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import main.java.mip.tema1.exercitiul2.company.departament.Departament;
import main.java.mip.tema1.exercitiul2.company.employee.Employee;
import main.java.mip.tema1.exercitiul2.enums.DepartamentName;
import main.java.mip.tema1.exercitiul2.populator.DepartamentAndEmployeePopulator;

public class Company {

	private ArrayList<Employee> employees;
	private ArrayList<Departament> departaments;
	
	public Company() {
		employees = new ArrayList<Employee>();
		departaments = new ArrayList<Departament>();
		DepartamentAndEmployeePopulator.populateDepartamentAndEmployeeLists(employees, departaments);
	}
	
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
	}
	public ArrayList<Departament> getDepartaments() {
		return departaments;
	}
	public void setDepartaments(ArrayList<Departament> departaments) {
		this.departaments = departaments;
	}

	public void sortEmployeesBySalary() {
		Collections.sort(employees);
		System.out.println("INFO: Employees has been sorted by salary.");
	}
	
	public void showEmployee() {
		System.out.println();
		System.out.println("Employees list: ");
		for(Employee employee: employees) {
			System.out.println(employee);
		}
		System.out.println();
	}
	

	private int getEmployeeAge(Employee employee) {
	
		int age = 0;
	
		final Calendar c = Calendar.getInstance();
		
		int currentYear = c.get(Calendar.YEAR);
		int currentMonth = c.get(Calendar.MONTH);
		int currentDay = c.get(Calendar.DAY_OF_MONTH);
		
		String birthdayYearString = employee.getCnp().substring(1, 3);
		int birthYear = Integer.parseInt(birthdayYearString);
		
		String birthdayMonthString = employee.getCnp().substring(3, 5); 
		int birthMonth = Integer.parseInt(birthdayMonthString);
		
		String birthDayString = employee.getCnp().substring(5, 7); 
		int birthDay = Integer.parseInt(birthDayString);
		
		boolean itWasHisBirthdayThisYear = currentMonth > birthMonth ? true :
			(currentMonth < birthMonth ? false : 
				(currentDay > birthDay ? true : false) ); 
		
		if(birthdayYearString.charAt(0)=='0')
			age = currentYear - (2000 + (birthdayYearString.charAt(1)-48));
		else
			age = currentYear - (1900 + birthYear);
		
		if(itWasHisBirthdayThisYear) {
			return age;
		} else {
			return age-1;
		}
	}
	
	private int getEmployeeYearsOfExperience(Employee employee) {
		
		int yearsOfExperience = 0;
		
		final Calendar c = Calendar.getInstance();
		
		int currentYear = c.get(Calendar.YEAR);
		int currentMonth = c.get(Calendar.MONTH);
		int currentDay = c.get(Calendar.DAY_OF_MONTH);
		
		c.setTime(employee.getDateOfEmployment());
		
		int employmentYear = c.get(Calendar.YEAR);
		int employmentMonth = c.get(Calendar.MONTH);
		int employmentDay = c.get(Calendar.DAY_OF_MONTH);
		
		boolean aYearHasPassed = currentMonth > employmentMonth ? true :
			(currentMonth < employmentMonth ? false : 
				(currentDay > employmentDay ? true : false) );
		
		yearsOfExperience = currentYear - employmentYear;
		
		if(aYearHasPassed) {
			return yearsOfExperience;
		} else {
			return yearsOfExperience -1;
			}
			
	}
	
	private void increaseEmployeeSalaryWith30Percentages(Employee employee) {
		Double newSalary = employee.getSalary() + (employee.getSalary()*20D)/100D;
		employee.setSalary(newSalary);
	}
	
	public void employeesOlderThan40YearsOldAndExperienceAtLeasts5Years() {
		System.out.println();
		System.out.println("Employees older than 40 year old with experience at leasts 5 years: ");
		
		for(Employee employee: employees) {
			int age = getEmployeeAge(employee);
			int experience = getEmployeeYearsOfExperience(employee);
		
			if(age >= 40 && experience >=5) {
				System.out.println(employee.getName() + " age: " + age + " experience: "+experience);
				System.out.println(employee);
			}
		}
		System.out.println();
	}
	
	public void salaryRaiseForAccountingEmployeesOlderThan30YearsOldAndExperienceAtLeast3Years() {
		System.out.println();
		System.out.println("Increasing salary for employees over 30 years old with more than 3 year experience: ");
		for(Employee employee: employees) {
			if(employee.getDepartament().getNume() == DepartamentName.ACCOUNTING) {
				int age = getEmployeeAge(employee);
				int experience = getEmployeeYearsOfExperience(employee);
			
				if(age >= 30 && experience >=3) {
					System.out.println(employee);
					System.out.println("Old salary: "+ employee.getSalary());
					increaseEmployeeSalaryWith30Percentages(employee);
					System.out.println("New salary: "+ employee.getSalary());
					System.out.println();
				}
			}
		}
		
	}
}
