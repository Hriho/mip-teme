package main.java.mip.tema1.exercitiul2.company.departament;

import main.java.mip.tema1.exercitiul2.enums.DepartamentName;

public class Departament {

	public Departament() {}
	
	public Departament(Long id, DepartamentName name) {
		this.id = id;
		this.name = name;
	}

	private Long id;
	
	private DepartamentName name;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public DepartamentName getNume() {
		return name;
	}
	public void setNume(DepartamentName name) {
		this.name = name;
	}
}
