package main.java.mip.tema1.exercitiul2.company.employee;

import java.util.Date;

import main.java.mip.tema1.exercitiul2.company.departament.Departament;

public class Employee implements Comparable<Employee>{

	public Employee() {}

	public Employee(Long id, String name, String cnp, Double salary, Departament departament, Date dateOfEmployment) {
		this.id = id;
		this.name = name;
		this.cnp = cnp;
		this.salary = salary;
		this.departament = departament;
		this.dateOfEmployment = dateOfEmployment;
	}
	
	private Long id;
	
	private String name;
	
	private String cnp;
	
	private Double salary;
	
	private Departament departament;
	
	private Date dateOfEmployment;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public Departament getDepartament() {
		return departament;
	}
	public void setDepartament(Departament departament) {
		this.departament = departament;
	}
	public Date getDateOfEmployment() {
		return dateOfEmployment;
	}
	public void setDateOfEmployment(Date dateOfEmployment) {
		this.dateOfEmployment = dateOfEmployment;
	}
	
	@Override
	public int compareTo(Employee employee) {
		if(getSalary() != employee.getSalary()) {
			return getSalary().compareTo(employee.getSalary());
		} else {
			return getName().compareTo(employee.getName());
		}
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", cnp=" + cnp + ", salary=" + salary + ", departament="
				+ departament.getNume() + ", dateOfEmployment=" + dateOfEmployment + "]";
	}
	
}
