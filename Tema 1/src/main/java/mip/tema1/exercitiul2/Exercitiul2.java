package main.java.mip.tema1.exercitiul2;

import main.java.mip.tema1.exercitiul2.company.Company;

public class Exercitiul2 {

	public static void main(String[] args) {
		
		Company company = new Company();
		company.showEmployee();
		company.sortEmployeesBySalary();
		company.showEmployee();
		company.employeesOlderThan40YearsOldAndExperienceAtLeasts5Years();
		company.salaryRaiseForAccountingEmployeesOlderThan30YearsOldAndExperienceAtLeast3Years();
		
	}
	
}
