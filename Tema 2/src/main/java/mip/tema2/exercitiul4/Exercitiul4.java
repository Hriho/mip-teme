package main.java.mip.tema2.exercitiul4;

import java.util.HashMap;
import java.util.Scanner;

public class Exercitiul4 {
	
	public static void populateHashMap(HashMap<String,Integer> hash) {
		for(int i=0;i<10;i++)
			hash.put("String"+(1+i), i+1);
	}

	public static void main(String[] args) {
		
		HashMap<String,Integer> hash = new HashMap<String,Integer>();
		populateHashMap(hash);
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		System.out.println("Insert the key that you want to heck if exist in Hash:");
		String key = input.next();
		if(hash.containsKey(key)) {
			System.out.println("This key exist in hash");
		} else {
			System.out.println("This key dosen't exist in hash.");
		}
		
		System.out.println("Insert the value that you want to heck if exist in Hash:");
		Integer value = input.nextInt();
		if(hash.containsValue(value)) {
			System.out.println("This value exist in hash");
		} else {
			System.out.println("This value dosen't exist in hash.");
		}
		
		System.out.println(hash.get("String2"));
		
		System.out.println(hash);
		
		hash.entrySet().forEach(entry->{
		    System.out.println(entry.getKey() + " " + entry.getValue());  
		 });
	}
	
}
