package main.java.mip.tema2.teste;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import main.java.mip.tema2.exercitul5.classes.Course;
import main.java.mip.tema2.exercitul5.classes.Student;
import main.java.mip.tema2.exercitul5.enums.Sex;
import main.java.mip.tema2.exercitul5.service.StudentService;

public class StudentServiceTest {

	StudentService studentService;
	Student student;
	Course course;
	
	@Before
	public void test0GenerateCourse() {
		course =null;
		course = new Course();
		course.setDescriere("test");
		course.setNota(10.0);
		course.setNrCredite(6);
		course.setNume("MIP");
	}
	
	@Before
	public void test1GenerateStudent() {
		studentService = new StudentService();
		student = null;
		student = new Student();
		student.setCnp("1990824080053");
		student.setNume("Hrihoreanu Radu");
		student.setDataNasterii("24.08.1999");
		student.setGen(Sex.MALE);
		ArrayList<Course> courses = new ArrayList<Course>();
		courses.add(course);
		student.setCourses(courses);
		assertTrue(student!=null);
	}
	
	@Test
	public void testNumberOfCourses() {
		assertEquals(student.getCourses().size(),1);
	}

	@Test
	public void testMedieRadu() {
		boolean mediiEgale= studentService.medieStudent(student) == 10.0;
		assertTrue(mediiEgale);
	}
	
	@Test
	public void testNumberOfCreditsForCourse() {
		int nrCredite = course.getNrCredite();
		assertEquals(nrCredite,6);
	}
	
	@Test
	public void testSumOfCreditsForStudent() {
		int sumOfCredits = studentService.sumaCreditelor(student);
		assertEquals(sumOfCredits,6);
	}
	
	@Test
	public void testStudentiBursieri() {
		ArrayList<Student> students = new ArrayList<Student>();
		students.add(student);
		ArrayList<Student> bursieri = studentService.studentiBaietiNeinscrisiLaSport(students);
		assertEquals(bursieri.size(),1);
	}
	
}