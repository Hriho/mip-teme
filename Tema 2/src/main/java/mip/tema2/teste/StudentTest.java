package main.java.mip.tema2.teste;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import main.java.mip.tema2.exercitul5.classes.Student;
import main.java.mip.tema2.exercitul5.enums.Sex;


public class StudentTest{

	Student student;
	
	@Before
	public void testGenerateStudent() {
		student = null;
		student = new Student();
		student.setCnp("1990824080053");
		student.setNume("Hrihoreanu Radu");
		student.setDataNasterii("24.08.1999");
		student.setGen(Sex.MALE);
		assertTrue(student!=null);
	}
	
	@Test
	public void testCnpLength() {
		assertEquals(student.getCnp().length(),13);
	}
	
	@Test
	public void testNameStartWithCapitalLetter() {
		assertTrue(65 <= student.getNume().charAt(0) &&
				student.getNume().charAt(0) <=92);
	}
	
	@Test
	public void testCnpOnlyContainsDigits() {
		for(int i=0;i<student.getCnp().length();i++) {
			assertTrue('0' <=  student.getCnp().charAt(i) &&
					student.getCnp().charAt(i) <='9');
		}
	}
	
	@Test
	public void testFirstDigitOfCnpCorrespondsWithGender() {
		if(student.getCnp().charAt(0)=='1') {
			assertEquals(Sex.MALE, student.getGen());
		} else if(student.getCnp().charAt(0)=='2') {
			assertEquals(Sex.FEMALE, student.getGen());
		}
	}	
	
	@Test
	public void testDayFromCnpCorrespondsWithBirthday() {
		assertEquals(student.getCnp().charAt(5),student.getDataNasterii().charAt(0));
		assertEquals(student.getCnp().charAt(6),student.getDataNasterii().charAt(1));
	}
}