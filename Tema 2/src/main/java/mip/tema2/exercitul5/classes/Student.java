package main.java.mip.tema2.exercitul5.classes;

import java.util.ArrayList;

import main.java.mip.tema2.exercitul5.enums.Sex;

public class Student extends Person{

	private ArrayList<Course> courses;

	public Student(String nume, String cnp, String dataNasterii, Sex gen, ArrayList<Course> courses) {
		super(nume, cnp, dataNasterii, gen);
		this.courses = new ArrayList<Course>(courses);
	}

	public Student() {	
		this.courses = new ArrayList<Course>();
	}

	public ArrayList<Course> getCourses() {
		return courses;
	}

	public void setCourses(ArrayList<Course> courses) {
		this.courses = courses;
	}
	

}
