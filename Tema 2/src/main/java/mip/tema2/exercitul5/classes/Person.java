package main.java.mip.tema2.exercitul5.classes;

import main.java.mip.tema2.exercitul5.enums.Sex;

public class Person {

	private String nume;
	
	private String cnp;
	
	private String dataNasterii;
	
	private Sex gen;
	
	public Person() {}

	public Person(String nume, String cnp, String dataNasterii, Sex gen) {
		this.nume = nume;
		this.cnp = cnp;
		this.dataNasterii = dataNasterii;
		this.gen = gen;
	}

	@Override
	public String toString() {
		return "Person [nume=" + nume + ", cnp=" + cnp + ", dataNasterii=" + dataNasterii + ", gen=" + gen + "]";
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getDataNasterii() {
		return dataNasterii;
	}

	public void setDataNasterii(String dataNasterii) {
		this.dataNasterii = dataNasterii;
	}

	public Sex getGen() {
		return gen;
	}

	public void setGen(Sex gen) {
		this.gen = gen;
	}
	
}
