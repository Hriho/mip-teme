package main.java.mip.tema2.exercitul5.classes;

public class Course {

	private String nume;
	
	private String descriere;
	
	private Integer nrCredite;
	
	private Double nota;

	public Course(String nume, String descriere, Integer nrCredite, Double nota) {
		this.nume = nume;
		this.descriere = descriere;
		this.nrCredite = nrCredite;
		this.nota = nota;
	}
	
	public Course() {}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	

	public String getDescriere() {
		return descriere;
	}

	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}

	public Integer getNrCredite() {
		return nrCredite;
	}

	public void setNrCredite(Integer nrCredite) {
		this.nrCredite = nrCredite;
	}

	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
		this.nota = nota;
	}

	@Override
	public String toString() {
		return "Course [nume=" + nume + ", descriere=" + descriere + ", nrCredite=" + nrCredite + ", nota=" + nota
				+ "]";
	}
	
}
