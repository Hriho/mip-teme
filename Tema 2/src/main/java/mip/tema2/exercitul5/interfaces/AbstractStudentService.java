package main.java.mip.tema2.exercitul5.interfaces;

import java.util.ArrayList;

import main.java.mip.tema2.exercitul5.classes.Student;

public interface AbstractStudentService {

	public int sumaCreditelor(Student student);
	public Double medieStudent(Student student);
	public ArrayList<Student> integralistStudents(ArrayList<Student> students);
	public ArrayList<Student> studentiCuBursa(ArrayList<Student> students);
	public ArrayList<Student> studentiBaietiNeinscrisiLaSport(ArrayList<Student> students);

	
}
