package main.java.mip.tema2.exercitul5.service;

import java.util.ArrayList;

import main.java.mip.tema2.exercitul5.classes.Course;
import main.java.mip.tema2.exercitul5.classes.Student;
import main.java.mip.tema2.exercitul5.enums.Sex;
import main.java.mip.tema2.exercitul5.interfaces.AbstractStudentService;

public class StudentService implements AbstractStudentService{
	
	public Double medieStudent(Student student) {
		Double medieStudent = 0.0;
		for(Course course: student.getCourses()) {
			medieStudent+=course.getNota();
		}
		medieStudent/=student.getCourses().size();
		return medieStudent;
	}
	
	public ArrayList<Student> integralistStudents(ArrayList<Student> students){
		ArrayList<Student> integralistStuents = new ArrayList<Student>();
		for(Student student: students) {
			boolean integralist = true;
			for(Course course: student.getCourses()) {
				if(course.getNota() < 5.0) {
					integralist = false;
					break;
				}
			}
			if(integralist) {
				integralistStuents.add(student);
			}
		}
		return integralistStuents;
	}
	
	public ArrayList<Student> studentiCuBursa(ArrayList<Student> students){
		ArrayList<Student> studentiCuBursa = this.integralistStudents(students);
		for(Student student: students) {
			if(this.medieStudent(student) >= 8.5) {
				studentiCuBursa.add(student);
			}
		}
		return studentiCuBursa;
	}
	
	public ArrayList<Student> studentiBaietiNeinscrisiLaSport(ArrayList<Student> students){
		ArrayList<Student> noSport = new ArrayList<Student>();
		
		for(Student student: students) {
			if(student.getGen() == Sex.MALE) {
			boolean hasSport = false;
			for(Course course: student.getCourses()) {
				if(course.getNume().equals("Sport")) {
					hasSport = true;
					break;
				}
			}
			if(!hasSport) {
				noSport.add(student);
			}
		}
		}
		return noSport;
	}

	public int sumaCreditelor(Student student) {
		int sum=0;
		
		for(Course course: student.getCourses()) {
			sum+=course.getNrCredite();
		}
	
		return sum;
	}
	
	
}
