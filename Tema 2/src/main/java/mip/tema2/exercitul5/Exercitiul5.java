package main.java.mip.tema2.exercitul5;

import java.util.ArrayList;
import java.util.Scanner;

import main.java.mip.tema2.exercitul5.classes.Course;
import main.java.mip.tema2.exercitul5.classes.Student;
import main.java.mip.tema2.exercitul5.enums.Sex;
import main.java.mip.tema2.exercitul5.service.StudentService;

public class Exercitiul5 {

	public static ArrayList<Course> courses = new ArrayList<Course>();
	public static ArrayList<Student> students =  new ArrayList<Student>();
	public static StudentService studentService = new StudentService();
	
	public static void addCourseToStudent() {
		System.out.println("Introduceti numele studentului caruia doriti sa ii adaugati un curs: ");
		for(Student student: students) {
			System.out.println(student.getNume());
		}
		System.out.println();
		System.out.println("Nume: ");
		boolean studentExist = false;
		Student currentStudent = null;
		
		while(!studentExist) {
			Scanner input = new Scanner(System.in);
			String numeStudent = input.nextLine();
		
			for(Student student: students) {
			 	if(student.getNume().equals(numeStudent)) {
			 		studentExist = true;
			 		currentStudent = student;
			 		break;
			 	}
		 	}
			if(!studentExist) {
				System.out.println("Nu exista un student cu numele "+numeStudent);
			}
		 }
		
		System.out.println("Introduceti numele cursului pe care doriti sa i-l adaugati studentului: ");
		for(Course course: courses) {
			System.out.println(course.getNume());
		}
		System.out.println();
		System.out.println("Nume: ");
		boolean courseExist = false;
		Course currentCourse = null;
		
		while(!courseExist) {
			Scanner input = new Scanner(System.in);
			String numeCurs = input.nextLine();
			
			for(Course course: courses) {
				if(course.getNume().equals(numeCurs)) {
					courseExist = true;
					currentCourse = course;
					break;
				}
			}
			
			if(!courseExist) {
				System.out.println("Nu exista un curs cu numele "+numeCurs);
			}
		}
		
		
		if(studentService.sumaCreditelor(currentStudent) + currentCourse.getNrCredite() > 60) {
			System.out.println("Nu puteti adauga acest curs deoarece ar depasi numarul de 60 de credite !");
			
		} else {
			System.out.println("Introduceti nota pe care a obitnut-o "+
					currentStudent.getNume()+ " la aceasta materie: ");
					Scanner input = new Scanner(System.in);
					Double nota = input.nextDouble();
					currentCourse.setNota(nota);
			currentStudent.getCourses().add(currentCourse);
			System.out.println("Cursul "+currentCourse.getNume()+ " a fost adaugat studentului "
			+ currentStudent.getNume());
			System.out.println();
		}
	}
	
	public static void createStudent() {
		Student student = new Student();
		
		String nume, cnp , dataNasterii, sex;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduceti numele: ");
		nume = input.nextLine();
		student.setNume(nume);
		
		System.out.println("Introduceti CNP-ul: ");
		cnp = input.next();
		student.setCnp(cnp);
		
		System.out.println("Introduceti data nasterii: ");
		dataNasterii = input.next(); 
		student.setDataNasterii(dataNasterii);
		
		boolean validSex = false;
		while(!validSex) {
			System.out.println("Introduceti sexul (M/F): ");
			sex = input.next();
			if(sex.equals("M")) {
				student.setGen(Sex.MALE);
				validSex = true;
			} else if(sex.equals("F")) {
				student.setGen(Sex.FEMALE);
				validSex = true;
			} else {
				System.out.println("Input invalid. Intoruceti litera M pentru Barbat si F pentru femeie !");
			}
		}
		
		students.add(student);
		System.out.println("A fost creeat studentul "+student.getNume());
		System.out.println();
		
	}
	
	public static void afisareListaStudenti() {
		System.out.println("Lista studenti: ");
		for(Student student: students) {
			System.out.println("Nume student "+student.getNume());
			System.out.println("CNP: "+student.getCnp());
			System.out.println("Data nasterii: "+student.getDataNasterii());
			System.out.println("Sex: " + student.getGen().toString());
			System.out.println();
			System.out.println();
		}
	}

	public static void createCourse() {
		String nume, descriere;
		Integer numarCredite;
		
		Course course = new Course();
		
		Scanner input = new Scanner(System.in);
		
		boolean invalideCourse = true;
		
		while(invalideCourse) {
			System.out.println("Introduceti numele materiei: ");
			nume = input.nextLine();
			boolean courseAlreadyExist = false;
			
			for(Course cours: courses){
				if(cours.getNume().equals(nume)){
					courseAlreadyExist = true;
					break;
				}
			}
			
			if(courseAlreadyExist){
			System.out.println("Aceasta materie exista deja in lista de cursuri !!!");
		}
		else {
			invalideCourse = false;
			course.setNume(nume);		
			}
		}
		
		System.out.println("Introduceti descrierea aplicatiei: ");
		descriere = input.nextLine();
		course.setDescriere(descriere);
		
		System.out.println("Introduceti numarul de credite: ");
		numarCredite = input.nextInt();
		course.setNrCredite(numarCredite);
		course.setNota(0.0);
		
		courses.add(course);
		System.out.println("Cursul "+course.getNume() + " a fost creeat.");
		System.out.println();
		
	}
	
	public static void afisareListaCursuri() {
		System.out.println("Lista cursuri: ");
		for(Course course: courses) {
			System.out.println("Nume curs: "+course.getNume());
			System.out.println("Descriere: "+course.getDescriere());
			System.out.println("Numar de credite: "+course.getNrCredite());
			System.out.println();
		}
		System.out.println();
	}

	public static void printMenuDetails() {
		System.out.println("WELCOME !!!");
		System.out.println("Introduceti numarul corespunzator operatiei pe care doriti sa o efectuati: ");
		System.out.println("1.Creeati un student");
		System.out.println("2.Creeati un curs");
		System.out.println("3.Adaugati un curs unui student");
		System.out.println("4.Afisati lista de studenti");
		System.out.println("5.Afisati lista de cursuri");
		System.out.println("6.Afisati studentii integralisti");
		System.out.println("7.Afisati studentii bursieri");
		System.out.println("8.Afisati baietii care nu au selectat sport");
		System.out.println("9.Afisati media unui student");
		System.out.println("10.Afisati suma creditlor cursurilor unui student");
		System.out.println("0.Exit");
		System.out.println();
	}
	
	public static void afisareStudentiIntegralisti() {
		System.out.println("Studentii integrilisti sunt: ");
		for(Student student: studentService.integralistStudents(students)) {
			System.out.println(student.getNume());
		}
		System.out.println();
	}
	
	public static void afisareStudentiBursieri() {
		System.out.println("Studentii bursieri sunt: ");
		for(Student student: studentService.studentiCuBursa(students)) {
			System.out.println(student.getNume());
		}
		System.out.println();
	}
	
	public static void noSportBoys() {
		System.out.println("Studentii baieti care nu au ales un sport sunt: ");
		for(Student student: studentService.studentiBaietiNeinscrisiLaSport(students)) {
			System.out.println(student.getNume());
		}
		System.out.println();
	}
	
	public static void afisareMedieStudent() {
		System.out.println("Introduceti numele studentului caruia doriti sa ii vedeti media: ");
		for(Student student: students) {
			System.out.println(student.getNume());
		}
		System.out.println();
		System.out.println("Nume: ");
		boolean studentExist = false;
		Student currentStudent = null;
		
		while(!studentExist) {
			Scanner input = new Scanner(System.in);
			String numeStudent = input.nextLine();
		
			for(Student student: students) {
			 	if(student.getNume().equals(numeStudent)) {
			 		studentExist = true;
			 		currentStudent = student;
			 		break;
			 	}
		 	}
			if(!studentExist) {
				System.out.println("Nu exista un student cu numele "+numeStudent);
			}
		 }
		
		System.out.println("Media studentului " +
		currentStudent.getNume()+" este "+ studentService.medieStudent(currentStudent));
		System.out.println();
	}
	
	public static void afisareSumaCrediteStudent() {
		System.out.println("Introduceti numele studentului caruia doriti sa ii calculati suma creditelor: ");
		for(Student student: students) {
			System.out.println(student.getNume());
		}
		System.out.println();
		System.out.println("Nume: ");
		boolean studentExist = false;
		Student currentStudent = null;
		
		while(!studentExist) {
			Scanner input = new Scanner(System.in);
			String numeStudent = input.nextLine();
		
			for(Student student: students) {
			 	if(student.getNume().equals(numeStudent)) {
			 		studentExist = true;
			 		currentStudent = student;
			 		break;
			 	}
		 	}
			if(!studentExist) {
				System.out.println("Nu exista un student cu numele "+numeStudent);
			}
		 }
		
		System.out.println("Suma creditelor studentului " +
		currentStudent.getNume()+ " este " +studentService.sumaCreditelor(currentStudent));
		System.out.println();
	}
	
	public static void main(String[] args) {

		printMenuDetails();
		
		Scanner scanner = new Scanner(System.in);
		int optiune = 1;
		while(optiune != 0) {
			System.out.println("Codul operatiei: ");
			optiune = scanner.nextInt();
			switch(optiune) {
				case 0: break;
				case 1: createStudent(); break;
				case 2: createCourse(); break;
				case 3: addCourseToStudent(); break;
				case 4: afisareListaStudenti(); break;
				case 5: afisareListaCursuri(); break;
				case 6: afisareStudentiIntegralisti(); break;
				case 7: afisareStudentiBursieri(); break;
				case 8: noSportBoys(); break;
				case 9: afisareMedieStudent(); break;
				case 10: afisareSumaCrediteStudent(); break;
				default:
					System.out.println("Aceasta operatie nu exista! Va rugam introduceti un cod existent !");
			}
		}
		
	}
	
}
