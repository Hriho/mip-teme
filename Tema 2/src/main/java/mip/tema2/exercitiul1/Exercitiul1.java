package main.java.mip.tema2.exercitiul1;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercitiul1 {
	
	public static void populateList(ArrayList<String> list) {
		for(int i=0;i<10;i++)
			list.add("String"+(i+1));
	}
	
	public static void printList(ArrayList<String> list) {
		for(String s: list)
			System.out.print(s+" ");
		System.out.println();
		System.out.println();
	}
	
	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		ArrayList<String> list = new ArrayList<String>();
		
		populateList(list);
		printList(list);
		
		list.add(0,"ElementAddedAtIndex0");
		printList(list);
		
		list.remove(3);
		printList(list);
		
		System.out.println("Introduceti elementul pe care il cautati in lista: ");
		String element = input.next();
		
		if(list.contains(element)) {
			System.out.println("Elementul "+ element +" exista in lista");
		} else {
			System.out.println("Elementul "+ element +" nu exista in lista");
		}
		
		ArrayList<String> elementsFrom2To5 = new ArrayList<String>(list.subList(2, 5));
		System.out.println("Elements from positions 2 to 5: ");
		printList(elementsFrom2To5);
		
		ArrayList<String> elementFrom3ToEnd = new ArrayList<String>(list.subList(3, list.size()));
		System.out.println("Elements from position 3 to end: ");
		printList(elementFrom3ToEnd);
	}
	
}
