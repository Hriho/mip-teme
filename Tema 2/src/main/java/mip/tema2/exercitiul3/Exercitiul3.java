package main.java.mip.tema2.exercitiul3;

import java.util.HashSet;

public class Exercitiul3 {
	
	public static void populateHashSet(HashSet<String> hash) {
		for(int i=0;i<10;i++)
			hash.add("String"+(i+1));
	}
	
	public static void printHash(HashSet<String> hash) {
		for(String s: hash)
			System.out.print(s+" ");
		System.out.println();
		System.out.println();
	}

	public static void main(String[] args) {
				
		HashSet<String> hash = new HashSet<String>();
		
		populateHashSet(hash);
		printHash(hash);
		
		System.out.println(hash.size());
		
		hash.clear();
		printHash(hash);
	}
	
	
}
