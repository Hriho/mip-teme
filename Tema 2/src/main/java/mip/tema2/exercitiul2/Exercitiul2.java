package main.java.mip.tema2.exercitiul2;

import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.Collections;


public class Exercitiul2 {

	public static void populateList(LinkedList<String> list) {
		for(int i=0;i<10;i++)
			list.add("String"+(i+1));
	}
	
	public static void printList(LinkedList<String> list) {
		for(String s: list)
			System.out.print(s+" ");
		System.out.println();
		System.out.println();
	}
	
	public static void printCommonElements(LinkedList<String> list1, LinkedList<String> list2) {
		System.out.println(list1.stream().filter(list2::contains).collect(Collectors.toList()));
	}
	
	public static void main(String[] args) {
		
		LinkedList<String> list = new LinkedList<String>();
		
		populateList(list);
		printList(list);
		
		list.add(0, "String0");
		list.add("String 11");
		printList(list);
		
		list.add(4, "String 4.2");
		printList(list);
		
		LinkedList<String> secondList = new LinkedList<String>();
		
		populateList(secondList);
		
		System.out.println("The common elements are: ");
		printCommonElements(list,secondList);
		
		
	}
	
}
